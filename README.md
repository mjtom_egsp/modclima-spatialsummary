# Spatial Summary - PostGIS

The goal of this project is produce a spatial summary from vector data provided.

The user allowed inputs are polygons inside:

* \*.shp.
* \*.geojson.
* \*.kml.

The summary will contain:

* *Area from the biggest polygon*
* *Area from the smallest polygon*
* *Sum from the area of all polygons*
* *The centroid from the feature class*

## Workflow behind it

![Workflow](./static/workflow.png)

* **1.** User will execute the script and pass as parameter the path for an spatial file accepted by the script. 

* **2.** The script will check if the spatial is valid (if it has features and if it's a polygon). If everything is correct the script will create a docker container.

* **3.** The container created will have PostgreSQL with PostGIS installed. The script will use OGR to transform the data to a table on PostGIS where will be possible to execute the querys and extract the summary we want.

* **4.** After the summary be generated, the values will be received by the script.

* **5.** If everything is correct, the script will destroy the docker container.

* **6.** Output the summary for the user via STDOUT and also generate a file with the result.

## Running the Script

### Required packages

To use the script you need to have the follow packages installed on your system:

* [GDAL](https://packages.ubuntu.com/xenial/gdal-bin)
* [Docker](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/)
* [python3-psycopg2](https://packages.ubuntu.com/xenial/python3-psycopg2) 
* [docker (pip package)](https://pypi.python.org/pypi/docker)

### Usage

```
$ python3 summarypostgis.py -h
usage: summarypostgis.py [-h] --file FILE

The script generate a summary from the spatial data you insert

optional arguments:
  -h, --help            show this help message and exit
  --file FILE, -f FILE  Insert the path for your spatial file.

```

### Notes

* The first time you run the script it can take some time because the script will download the image from dockerhub.
* You'll use root or sudo to run this script. Create docker containers require root permission.
