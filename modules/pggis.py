#!/usr/bin/python3

import psycopg2, time

def pgquery(ip, query):
    """
    Use some querys to generate a small summary from
    the spatial file.
    """
    connparams = "dbname=gis user=docker password=docker host=%s" % (ip)
    conn = psycopg2.connect(connparams)
    cur = conn.cursor()
    cur.execute(query)
    result = cur.fetchone()
    cur.close()
    conn.close()
    return result[0]

def spatialquery(ip):
    """
    Use some querys to generate a small summary from
    the spatial file.
    """
    summary = {}
    time.sleep(10)
    sumquery = "SELECT SUM(ST_AREA(ST_Transform(geom, \
                3857))) AS area FROM temptable;"
    largestquery = "SELECT ST_Area(ST_Transform(geom, 3857)) AS area FROM \
                    temptable ORDER BY area DESC;"
    smallestquery = "SELECT ST_Area(ST_Transform(geom, 3857)) AS area FROM \
                     temptable ORDER BY area ASC;"
    centroidquery = "SELECT ST_AsText(ST_Centroid(ST_Union(geom))) FROM \
                     temptable"
    summary['soma'] = pgquery(ip, sumquery)
    summary['maior'] = pgquery(ip, largestquery)
    summary['menor'] = pgquery(ip, smallestquery)
    summary['centroid'] = pgquery(ip, centroidquery)
    return summary
