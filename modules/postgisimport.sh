#!/bin/bash

ogr2ogr -f 'PostgreSQL' PG:"host=$1 user=docker password=docker dbname=gis" $2 -lco GEOMETRY_NAME=geom -lco FID=gid -lco PRECISION=no -nlt PROMOTE_TO_MULTI -nln temptable -select geom -overwrite -skipfailures
