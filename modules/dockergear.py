#!/usr/bin/python3

import docker, subprocess

class DockerUtils():
    """
    This class will handle the creating, the execution of commands
    on the docker and the destruction of the container.
    """

    def __init__(self):
        """
        Create variables that will be used by GDAL and for query
        PostGIS too.
        """
        self.client = docker.from_env()
        self.dockerip = None
        self.dockerid = None

    def dockerup(self):
        """
        Create the container and update the dockerid and dockerip.
        """
        container = self.client.containers.run(
                    "kartoza/postgis:latest", detach=True)
        self.dockerid = container.id
        command = ['bash', 'modules/dockerip.sh', self.dockerid]
        dockerinspect = subprocess.Popen(command, stdout=subprocess.PIPE)
        result = dockerinspect.stdout.read()
        self.dockerip = result.decode('ascii').replace('\n','')

    def dockerkill(self):
        """
        Kill the docker container
        """
        command = ['docker', 'rm', '-f', self.dockerid]
        try:
            dockerremove = subprocess.Popen(command, stdout=subprocess.PIPE)
        except Exception as e:
            pass
        else:
            return True
