#!/usr/bin/python3

import subprocess, time

def filetopostgis(filename, ip):
    """
    Convert the data to a PostGIS table.
    The tablename is temptable
    """
    time.sleep(45)
    command = ['bash', 'modules/postgisimport.sh', ip, filename]
    subprocess.Popen(command, stdout=subprocess.PIPE)
