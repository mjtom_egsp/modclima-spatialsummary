#!/usr/bin/python3

import ogr, sys

def checkextension(filepath):
    """
    Check if the file has the extension *.shp, *.geojson or *.kmz.
    """
    extension = filepath.split('.')[-1]
    if extension in ["geojson", "kmz", "shp"]:
        return [True, extension]  
    else:
        message = "This is not a valid format\n"
        message += "Check the README.md to see the valid formats"
        print(message)
        sys.exit(1)

def ispolygon(filepath):
    """
    Check if the file is a polygon or multipolygon feature.
    Other polygons are not accepted because the script won't be able
    to generate area calcs for lines and points.
    This function also check if the feature class have at least one 
    feature.
    """
    data = ogr.Open(filepath, 0)
    if not data:
        message = "Unable to open the file. Verify if it's a valid file"
        print(message)
        sys.exit(1)
    layers = data.GetLayer()
    layer = [layer for layer in layers]
    geometry = layer[0].GetGeometryRef()
    features_count = geometry.GetGeometryCount()
    geometry_type = geometry.GetGeometryName()
    if features_count == 0:
        print("This file has no features. Check if that's correct.")
        sys.exit(1)
    else:
        if geometry_type.lower() in ['polygon', 'multipolygon']:
            return True
        else:
            message = "The script don't accept this geometry type\n"
            message += "Please, inser POLYGON or MULTIPOLYGON files."
            print(message)
            sys.exit(1)

def isvalid(filepath):
    """
    This function will run some actions on the file to check if it's
    a valid file.
    """
    getextension = checkextension(filepath)
    extension = getextension[1]
    if getextension[0]:
        if ispolygon(filepath):
            return [True, extension]
