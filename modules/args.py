#!/usr/bin/python

import argparse

def getargs():
    """
    Get the arguments passed by the use. The user only can pass one 
    parameters, that is the path for the file he want the summary.
    """
    description = "The script generate a summary from the"
    description += " spatial data you insert"
    parser = argparse.ArgumentParser(description = description)
    argshelp = "Insert the path for your spatial file."
    parser.add_argument('--file', '-f', help = argshelp, required = True)
    args = parser.parse_args()
    return args
