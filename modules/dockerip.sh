#!/bin/bash

## Get Docker IP
docker inspect $1 | grep '"IPAddress"' | head -n1 | cut -d'"' -f4 
