#!/usr/bin/python3

from modules import args, checkdata, dockergear, convertdata, \
                    pggis
import json

if __name__ == '__main__':
    """
    Main area from the program. The main workflow from the script
    will be placed here
    """
    filename = args.getargs().file
    datastatus = checkdata.isvalid(filename)
    dockerpostgis = dockergear.DockerUtils()
    dockerpostgis.dockerup()
    postgisip = dockerpostgis.dockerip
    convert_postgis = convertdata.filetopostgis(filename, postgisip)
    summary = pggis.spatialquery(str(postgisip))
    dockerpostgis.dockerkill()
    with open('results.json', 'w') as file_object:
        json.dump(summary, file_object)
    print(json.dumps(summary, indent=4))
    
